package com.dao;
import com.dao.GroceryListDaoImpl;
import com.dao.ItemDaoImpl;
import com.model.GroceryList;

public class DaoTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GroceryListDaoImpl dao = new GroceryListDaoImpl();
		ItemDaoImpl idao = new ItemDaoImpl();
		dao.createList("testing");
		GroceryList list = dao.getList("myfirstlist");
		System.out.println(list.getId());
		System.out.println(list.getList().get(0).getName());
		System.out.println(list.getList().get(0).getPrice());
		System.out.println(list.getList().get(0).getType());
		System.out.println(list.getList().get(1).getName());
		dao.deleteList("testing");
		//idao.createItem("testing", 1, "test", 1);
		idao.updateItem(1, "test", 2, "test", 1);
		idao.deleteItem(5);

	}

}
