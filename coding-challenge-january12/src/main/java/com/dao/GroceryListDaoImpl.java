package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.model.GroceryList;
import com.model.Item;

public class GroceryListDaoImpl implements GroceryListDao{
	
	String url = "";
	String username = "";
	String password = "";
	
	
	static {
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("try static block has fired");
        } catch (ClassNotFoundException e) {
            System.out.println("THE STATIC BLOCK HAS FAILED");
            e.printStackTrace();
        }
    }
	
	public GroceryListDaoImpl() {

		
	}



	@Override
	public void createList(String name) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "INSERT INTO List values (DEFAULT, " + "'" +name+ "'" + ")";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public GroceryList getList(String name) {
		GroceryList list = new GroceryList();
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "SELECT * FROM list WHERE listname =" + "'" +name+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); 
			while(rs.next()) {
				list.setId(rs.getInt(1));
				list.setName(rs.getString(2));
			}
			
			String sql1 = "SELECT * FROM items WHERE listid =" + "'" +list.getId()+ "'";
			
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			
			ResultSet rs1 = ps1.executeQuery();
			ArrayList<Item> items = new ArrayList<Item>();
			while(rs1.next()) {
				Item item = new Item();
				item.setId(rs1.getInt(1));
				item.setName(rs1.getString(2));
				item.setPrice(rs1.getInt(3));
				item.setType(rs1.getString(4));
				items.add(item);
			}
			
			list.setList(items);

			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		System.out.println("in dao here is list name:");
		System.out.println(list.getName());
		if(list.getName()==null)
			return null;
		else
			return list;
	}

	@Override
	public void updateList(int id, String name) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "UPDATE list SET listname=" + "'" +name  + "'"+"  WHERE id = "+ +id;
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteList(String name) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "delete from List where listname = " + "'" +name+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

}
