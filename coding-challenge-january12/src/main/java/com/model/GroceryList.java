package com.model;

import java.util.ArrayList;

public class GroceryList {
	private int id;
	private String name;
	private ArrayList<Item> list;
	
	public GroceryList() {
		
	}
	public GroceryList(int id, String name, ArrayList<Item> list) {
		this.setId(id);
		this.setName(name);
		this.setList(list);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ArrayList<Item> getList() {
		return list;
	}
	public void setList(ArrayList<Item> list) {
		this.list = list;
	}
}
