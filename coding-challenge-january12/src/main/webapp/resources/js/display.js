window.onload = function() {
	
	//I don't want to add an event listener because the page load IS THE EVENT
	//in your project 1, you'll be going to the server here to get the session data...because THAT is who
	// is currently logged in
	getAllReimbursements();
}

let myUserInformation ={};

function getAllReimbursements() {
	fetch(
			'http://localhost:9020/coding-challenge-january12/api/Ajax/getByName')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				myUserInformation=daSecondResponse;
				ourDOMManipulation(daSecondResponse);
			})


}

function ourDOMManipulation(ourJSON) {

	for (let i = 0; i < ourJSON.length; i++) {
		
		
		// all creations
		let newTR = document.createElement("tr");
		let newTD0 = document.createElement("td");
		
		let newTD1 = document.createElement("td");
		let newTD2 = document.createElement("td");
		let newTD3 = document.createElement("td");
		
		
		// population creations
		console.log(ourJSON[i].reimb_id);
		let myText1 = document.createTextNode(ourJSON[i].id);
		let myText2 = document.createTextNode(ourJSON[i].name);
		let myText3 = document.createTextNode(ourJSON[i].price);
		let myText4 = document.createTextNode(ourJSON[i].type);
		
		newTD0.appendChild(myText1);
		newTD1.appendChild(myText2);
		newTD2.appendChild(myText3);
		newTD3.appendChild(myText4);
		
		newTR.appendChild(newTD0);
		newTR.appendChild(newTD1);
		newTR.appendChild(newTD2);
		newTR.appendChild(newTD3);
		
		let newSelectionTwo = document.querySelector("#villTableBody");
		newSelectionTwo.appendChild(newTR);

	}
}
