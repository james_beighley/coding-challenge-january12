package com.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.controller.ListController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.controller.ListController;


public class RequestHelper {

	public static String process(HttpServletRequest request) throws JsonProcessingException, IOException {
		System.out.println("\t\tIn RequestHelper POST");
		System.out.println(request.getRequestURI());
		
		switch(request.getRequestURI()) {
		/*case "/project-1/forwarding/login":
			System.out.println("case1");
			return LoginController.login(request);
		case "/project-1/forwarding/home":
			System.out.println("case 2");
			return HomeController.home(request);
		case "/project-1/forwarding/new":
			return ReimbursementController.createReimbursement(request);
		case "/project-1/forwarding/resolve":
			return ReimbursementController.resolveReimbursement(request);
		case "/project-1/forwarding/signout":
			System.out.println("in signout forwarding servlet case");
			return LoginController.logout(request);*/
		case "/coding-challenge-january12/forwarding/new":
			System.out.println("block1");
			return ListController.newList(request);
		case "/coding-challenge-january12/forwarding/get":
			System.out.println("block2");
			return ListController.getList(request);
		case "/coding-challenge-january12/forwarding/exit":
			System.out.println("block3");
			return ListController.back(request);
		case "/coding-challenge-january12/forwarding/deletelist":
			return ListController.deleteList(request);
		case "/coding-challenge-january12/forwarding/deleteItem":
			return ListController.deleteItem(request);
		case "/coding-challenge-january12/forwarding/updateItem":
			return ListController.deleteItem(request);
		case "/coding-challenge-january12/forwarding/createItem":
			return ListController.createItem(request);
		case "/coding-challenge-january12/forwarding/updatelist":
			return ListController.updateList(request);
		default:
			System.out.println("bad checkpoint");
			return "/resources/html/nolist.html";
		}
	}
}
