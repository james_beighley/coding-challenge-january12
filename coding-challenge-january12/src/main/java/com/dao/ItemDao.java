package com.dao;

import com.model.Item;

public interface ItemDao {
	public void createItem(String name, int price, String type, int listid);
	public Item getItem(String name);
	public void deleteItem(int Id);
	void updateItem(int id, String name, int price, String type, int listid);
}
