package com.service;
import com.dao.GroceryListDaoImpl;
import com.dao.ItemDaoImpl;
import com.model.GroceryList;
public class GroceryListService {
	GroceryListDaoImpl grocerydao = new GroceryListDaoImpl(); 
	ItemDaoImpl itemdao = new ItemDaoImpl();
	public GroceryList getList(String name) {
		return grocerydao.getList(name);
	}
	public void createList(String name) {
		grocerydao.createList(name);
	}
	public void deleteList(String name) {
		grocerydao.deleteList(name);
	}
	public void deleteItem(int id) {
		itemdao.deleteItem(id);
	}
	public void updateItem(int id, String name, int price, String type, int listid) {
		itemdao.updateItem(id, name, price, type, listid);
	}
	public void createItem(String name, int price, String type, int listid) {
		itemdao.createItem(name, price, type, listid);
	}
	public void deleteItemsByList(int Id) {
		itemdao.deleteItemsByList(Id);
	}
	public void updateList(int id, String name) {
		grocerydao.updateList(id, name);
	}
}
