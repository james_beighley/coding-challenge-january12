package com.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.controller.ListController;

public class AjaxRequestHelper {

	public static void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println(request.getRequestURI());

		switch (request.getRequestURI()) {
		case "/coding-challenge-january12/api/Ajax/getByName":
			System.out.println("in ajax request block AAAAAA");
			ListController.getByName(request,response);
			System.out.println("exit out of getbyname");
			break;
		default:
			response.getWriter().println("null");
		}
	}
}
