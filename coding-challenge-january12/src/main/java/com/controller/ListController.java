package com.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.model.GroceryList;
import com.model.Item;
import com.service.GroceryListService;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ListController {
	public static String newList(HttpServletRequest request) {
		GroceryListService service = new GroceryListService();
		String listname = request.getParameter("password");
		System.out.println(listname);
		service.createList(listname);
		GroceryList newList = service.getList(listname);
		HttpSession session = request.getSession();
		session.setAttribute("listname", listname);
		session.setAttribute("listid", newList.getId());
		return "/resources/html/display.html";
	}
	
	
	public static String getList(HttpServletRequest request) {
		GroceryListService service = new GroceryListService();
		String listname = request.getParameter("password");
		GroceryList list = service.getList(listname);
		if(list==null || listname==null)
			return "resources/html/nolist.html";
		HttpSession session = request.getSession();
		session.setAttribute("listname", listname);
		session.setAttribute("listid", list.getId());
		return "/resources/html/display.html";
	}
	
	
	public static void getByName(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		GroceryListService service = new GroceryListService();
		System.out.println("trying to fetch items by listname:");
		System.out.println((String)request.getSession().getAttribute("listname"));
		GroceryList list = service.getList((String)request.getSession().getAttribute("listname"));
		String tosend = "[";
		
		for(int i = 0; i<list.getList().size(); i++) {
			tosend+= new ObjectMapper().writeValueAsString(list.getList().get(i));
			if(i<list.getList().size()-1)
				tosend+= ",";
		}
		tosend+= "]";
		System.out.println(tosend);
		System.out.println(tosend.length());
		response.getWriter().write(tosend);
	}
	
	
	public static String deleteList(HttpServletRequest request) {
		System.out.println("in delete list method");
		GroceryListService service = new GroceryListService();
		service.deleteList((String)request.getSession().getAttribute("listname"));
		service.deleteItemsByList((Integer)request.getSession().getAttribute("listid"));
		request.getSession().invalidate();
		return "/index.html";
	}
	
	public static String deleteItem(HttpServletRequest request) {
		GroceryListService service = new GroceryListService();
		service.deleteItem(Integer.valueOf(request.getParameter("id")));
		return "/resources/html/display.html";
	}
	
	public static String updateItem(HttpServletRequest request) {
		GroceryListService service = new GroceryListService();
		service.updateItem(Integer.valueOf(request.getParameter("id")),request.getParameter("name"),Integer.valueOf(request.getParameter("id")),request.getParameter("type"), (Integer)request.getSession().getAttribute("listid"));
		return "/resources/html/display.html";
	}
	public static String createItem(HttpServletRequest request) {
		GroceryListService service = new GroceryListService();
		service.createItem(request.getParameter("name"),Integer.valueOf(request.getParameter("price")),request.getParameter("type"), (Integer)request.getSession().getAttribute("listid"));
		return "/resources/html/display.html";
	}
	
	public static String updateList(HttpServletRequest request) {
		GroceryListService service = new GroceryListService();
		service.updateList((Integer)request.getSession().getAttribute("listid"), request.getParameter("name"));
		return "/resources/html/display.html";
	}
	

	public static String back(HttpServletRequest request) {
		request.getSession().invalidate();
		System.out.println("Session has been invalidated");
		return "/index.html";

	}
}
