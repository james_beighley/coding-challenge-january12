package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.model.Item;

public class ItemDaoImpl implements ItemDao {
	
	String url = "";
	String username = "";
	String password = "";
	
	
	static {
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("try static block has fired");
        } catch (ClassNotFoundException e) {
            System.out.println("THE STATIC BLOCK HAS FAILED");
            e.printStackTrace();
        }
    }

	@Override
	public void createItem(String name, int price, String type, int listid) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "INSERT INTO Items values (DEFAULT, " + "'" +name+ "'" + "," +price+"," + "'"+type+ "'" + ","+listid+ ")";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Item getItem(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateItem(int id, String name, int price, String type, int listid) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "UPDATE items SET foodname=" + "'" +name  + "'" + ", price=" + "'" +price  + "'" + ", itemtype = " + "'" +type  + "'"+"  WHERE id = "+ "'" +id +"'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteItem(int Id) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "delete from items where id = " +Id;
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}
	
	public void deleteItemsByList(int Id) {
		try(Connection conn = DriverManager.getConnection(url, username,
				password)){
			
			String sql = "delete from items where listid = " +Id;
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.executeUpdate(); //<--query not update
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
