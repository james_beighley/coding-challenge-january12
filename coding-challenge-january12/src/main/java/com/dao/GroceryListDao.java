package com.dao;
import com.model.GroceryList;

public interface GroceryListDao {
	public void createList(String name);
	public GroceryList getList(String name);
	public void deleteList(String name);
	void updateList(int id, String name);
}
